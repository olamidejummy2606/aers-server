import { Request, Response } from "express";
import { BaseController } from "utils/baseController";
import { ADMIN_EMAIL, ADMIN_PASSWORD } from "utils/settings";

export default async function AdminLoginController(
	req: Request,
	res: Response
) {
	const user = req?.body;

	if (user?.email !== ADMIN_EMAIL && user?.password !== ADMIN_PASSWORD) {
		return BaseController.unauthorized(res, {
			message: "Invalid admin details",
			error: "Invalid admin details",
			status: false,
		});
	}

	BaseController.ok(res, {
		message: "Successful",
		status: true,
	});
}
