import { MigrationInterface, QueryRunner } from "typeorm";

export class Migrations1717451673219 implements MigrationInterface {
    name = 'Migrations1717451673219'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "stages" ADD "sn" integer`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "stages" DROP COLUMN "sn"`);
    }

}
